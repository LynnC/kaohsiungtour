package com.lynn.kotlintest.ui

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.ViewPagerActions.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.viewpager.widget.ViewPager
import com.lynn.kotlintest.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.core.AllOf.allOf
import org.junit.Rule
import org.junit.Test

/**
 * Created by lynnchen on 2019/4/5.
 */
@LargeTest
class MainActivityTest {

    /**
     * [IntentsTestRule] is an [ActivityTestRule] which inits and releases Espresso
     * Intents before and after each test run.
     *
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @get:Rule
    var activityTestRule = ActivityTestRule(MainActivity::class.java, false, true)


    /**
     * Returns a matcher that matches [ViewPager] that are selected the specific position.
     */
    private fun withViewAtPosition(position: Int): Matcher<View> {
        return object : BoundedMatcher<View, ViewPager>(ViewPager::class.java) {
            override fun describeTo(description: Description) {}

            override fun matchesSafely(viewPager: ViewPager): Boolean {
                return viewPager.currentItem == position
            }
        }
    }

    @Test
    fun swipeViewPager_checkIfTabIsCorrectlySelected() {
        val tabLayoutMatcher = isDescendantOfA(withId(R.id.tabLayout))
        val tabCityGuide = onView(allOf(withText(R.string.cityguide), tabLayoutMatcher))
        val tabShop = onView(allOf(withText(R.string.shop), tabLayoutMatcher))
        val tabEat = onView(allOf(withText(R.string.eat), tabLayoutMatcher))

        val viewPager = onView(withId(R.id.viewPager))

        // At beginning
        tabCityGuide.check(matches(isSelected()))

        // Start to swipe left to right
        viewPager.perform(swipeLeft())
        tabShop.check(matches(isSelected()))

        viewPager.perform(swipeLeft())
        tabEat.check(matches(isSelected()))

        viewPager.perform(swipeLeft())
        tabEat.check(matches(isSelected()))

        // Start to swipe right to left
        viewPager.perform(swipeRight())
        tabShop.check(matches(isSelected()))

        viewPager.perform(swipeRight())
        tabCityGuide.check(matches(isSelected()))

        viewPager.perform(swipeRight())
        tabCityGuide.check(matches(isSelected()))

        // Scroll to specific index of viewPage.
        viewPager.perform(scrollToPage(2))
        tabEat.check(matches(isSelected()))

        viewPager.perform(scrollToPage(1))
        tabShop.check(matches(isSelected()))

        viewPager.perform(scrollToPage(0))
        tabCityGuide.check(matches(isSelected()))

        viewPager.perform(scrollToLast())
        tabEat.check(matches(isSelected()))

        viewPager.perform(scrollToFirst())
        tabCityGuide.check(matches(isSelected()))
    }

    @Test
    fun clickTab_checkIfViewPagerIsShowingChanged() {
        val tabLayoutMatcher = isDescendantOfA(withId(R.id.tabLayout))
        val tabCityGuide = onView(allOf(withText(R.string.cityguide), tabLayoutMatcher))
        val tabShop = onView(allOf(withText(R.string.shop), tabLayoutMatcher))
        val tabEat = onView(allOf(withText(R.string.eat), tabLayoutMatcher))

        val viewPager = onView(withId(R.id.viewPager))

        tabCityGuide.perform(click())
        viewPager.check(matches(withViewAtPosition(0)))

        tabShop.perform(click())
        viewPager.check(matches(withViewAtPosition(1)))

        tabEat.perform(click())
        viewPager.check(matches(withViewAtPosition(2)))

        tabCityGuide.perform(click())
        viewPager.check(matches(withViewAtPosition(0)))

        tabShop.perform(click())
        viewPager.check(matches(withViewAtPosition(1)))

        tabEat.perform(click())
        viewPager.check(matches(withViewAtPosition(2)))
    }

}