package com.lynn.kotlintest.scheduler


import androidx.annotation.NonNull
import io.reactivex.Scheduler

/**
 * Created by lynnchen on 2019/4/4.
 */

interface BaseSchedulerProvider {

    @NonNull
    fun computation(): Scheduler

    @NonNull
    fun io(): Scheduler

    @NonNull
    fun ui(): Scheduler
}
