package com.lynn.kotlintest.data.source

import com.lynn.kotlintest.data.Data

import androidx.annotation.NonNull
import io.reactivex.Flowable

/**
 * Created by lynnchen on 2019/4/4.
 */

interface DataSource {

    fun getCityGuideDataList(count: Int): Flowable<List<Data>>

    fun getCityGuideDataList(count: Int, @NonNull lastDataId: String): Flowable<List<Data>>
}
