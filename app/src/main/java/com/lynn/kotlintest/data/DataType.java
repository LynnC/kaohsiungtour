package com.lynn.kotlintest.data;

/**
 * Created by lynnchen on 2019/4/3.
 */

public @interface DataType {
    int INFO = 0;
    int IMAGE = 1;
}
