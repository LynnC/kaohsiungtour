package com.lynn.kotlintest.data.source.remote


import com.lynn.kotlintest.data.Data
import com.lynn.kotlintest.data.source.DataSource

import javax.inject.Singleton

import androidx.annotation.NonNull
import io.reactivex.Flowable

/**
 * Created by lynnchen on 2019/4/4.
 */

@Singleton
class RemoteDataSource : DataSource {

    override fun getCityGuideDataList(count: Int): Flowable<List<Data>> {
        throw UnsupportedOperationException("Not implemented yet.")
    }

    override fun getCityGuideDataList(count: Int, @NonNull lastDataId: String): Flowable<List<Data>> {
        throw UnsupportedOperationException("Not implemented yet.")
    }
}
