package com.lynn.kotlintest.data.source


import com.lynn.kotlintest.data.Data

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.annotation.VisibleForTesting
import com.lynn.kotlintest.OpenForTesting
import io.reactivex.Flowable

/**
 * Created by lynnchen on 2019/4/4.
 */

@OpenForTesting
class Repository private constructor(private val remoteDataSource: DataSource) {

    fun getCityGuideDataList() = getCityGuideDataList(DEFAULT_COUNT)

    fun getCityGuideDataList(count: Int): Flowable<List<Data>> {
        return remoteDataSource.getCityGuideDataList(count)
    }

    fun getCityGuideDataList(@NonNull lastDataId: String): Flowable<List<Data>> {
        return getCityGuideDataList(DEFAULT_COUNT, lastDataId)
    }

    fun getCityGuideDataList(count: Int, @NonNull lastDataId: String): Flowable<List<Data>> {
        return remoteDataSource.getCityGuideDataList(count, lastDataId)
    }

    companion object {

        private const val DEFAULT_COUNT = 10

        @Nullable
        private var INSTANCE: Repository? = null

        fun getInstance(remoteDataSource: DataSource): Repository {
            if (INSTANCE == null) {
                INSTANCE = Repository(remoteDataSource)
            }
            return INSTANCE as Repository
        }

        /**
         * Used to force [.getInstance] to create a new instance
         * next time it's called when testing.
         */
        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
