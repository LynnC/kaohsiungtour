package com.lynn.kotlintest.data

import com.google.gson.annotations.SerializedName

/**
 * Created by lynnchen on 2019/4/4.
 */

class Data(id: String, title: String? = null, description: String? = null, imageUri: String) {

    var id: String = id
        private set
    var title: String? = title
        private set
    var description: String? = description
        private set
    @SerializedName("image_url")
    var imageUri: String = imageUri
        private set

    @DataType
    var type = DataType.IMAGE
        get() = if (title.isNullOrBlank()) DataType.IMAGE else DataType.INFO
        private set

    override fun equals(obj: Any?) =
            if (this === obj) true
            else if (obj == null || obj !is Data) false
            else if (type != obj.type || id != obj.id || imageUri != obj.imageUri) false
            else if (type == DataType.IMAGE) true
            else (title != null && title == obj.title
                    && description != null && description == obj.description)

}
