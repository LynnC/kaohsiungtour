package com.lynn.kotlintest.di

import android.content.Context
import com.lynn.kotlintest.OpenForTesting
import com.lynn.kotlintest.data.source.DataSource
import com.lynn.kotlintest.data.source.Repository
import com.lynn.kotlintest.scheduler.BaseSchedulerProvider
import com.lynn.kotlintest.scheduler.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by lynnchen on 2019/4/7.
 */

@Module
@OpenForTesting
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext() = context

    @Provides
    fun provideTasksRepository(dataSource: DataSource) = Repository.getInstance(dataSource)

    @Provides
    @Singleton
    fun provideSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider()

}
