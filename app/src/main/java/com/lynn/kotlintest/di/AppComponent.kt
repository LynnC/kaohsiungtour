package com.lynn.kotlintest.di

import android.content.Context
import com.lynn.kotlintest.data.source.Repository
import com.lynn.kotlintest.ui.cityguide.CityGuidePresenterImpl
import dagger.Component
import javax.inject.Singleton

/**
 * Created by lynnchen on 2019/4/7.
 */

@Singleton
@Component(modules = [(AppModule::class), (DataSourceModule::class)])
interface AppComponent {

    fun inject(cityGuidePresenterImpl: CityGuidePresenterImpl)

    fun context(): Context

    fun repository(): Repository
}
