package com.lynn.kotlintest.ui.cityguide

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lynn.kotlintest.R
import com.lynn.kotlintest.data.Data
import com.lynn.kotlintest.ui.component.loadmore.InfiniteScrollListener
import kotlinx.android.synthetic.main.fragment_cityguide.view.*


/**
 * Created by lynnchen on 2019/4/4.
 */

class CityGuideFragment : Fragment(), CityGuideView, SwipeRefreshLayout.OnRefreshListener {

    private var presenter: CityGuidePresenter? = null

    private lateinit var infiniteScrollListener: InfiniteScrollListener

    private val adapter = CityGuideAdapter()

    private lateinit var swipeRefreshLayoutCityGuide: SwipeRefreshLayout
    private lateinit var rvCityGuide: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_cityguide, container, false)

        val linearLayoutManager = LinearLayoutManager(context)

        infiniteScrollListener = object : InfiniteScrollListener(linearLayoutManager) {
            override fun onLoadMore() {
                val lastData = adapter.getItem(adapter.itemCount - 1)
                if (lastData != null) {
                    presenter?.loadMore(lastData.id)
                }
            }
        }

        swipeRefreshLayoutCityGuide = rootView.swipeRefreshLayoutCityGuide;
        swipeRefreshLayoutCityGuide.setOnRefreshListener(this)

        rvCityGuide = rootView.rvCityGuide
        rvCityGuide.adapter = adapter
        rvCityGuide.hasFixedSize()
        rvCityGuide.layoutManager = linearLayoutManager
        rvCityGuide.addOnScrollListener(infiniteScrollListener)

        presenter!!.subscribe()

        return rootView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter!!.unsubscribe()
        rvCityGuide.removeOnScrollListener(infiniteScrollListener)
    }

    override fun setPresenter(presenter: CityGuidePresenter) {
        this.presenter = presenter
    }

    override fun isAttached() = isAdded

    override fun appendDataList(dataList: List<Data>) {
        adapter.appendItems(dataList)
    }

    override fun replaceDataList(dataList: List<Data>) {
        adapter.replaceItems(dataList)
    }

    override fun onRefresh() {
        presenter?.refreshDataList()
    }

    override fun showRefreshing() {
        swipeRefreshLayoutCityGuide.isRefreshing = true
    }

    override fun showLoadingMore() {
        adapter.showLoadingView(rvCityGuide)
    }

    override fun dismissRefreshing() {
        swipeRefreshLayoutCityGuide.isRefreshing = false
    }

    override fun dismissLoadingMore() {
        adapter.dismissLoadingView()
    }

    override fun networkNotAvailable() {
        Toast.makeText(context, "Network is not available.", Toast.LENGTH_SHORT).show()
    }
}
