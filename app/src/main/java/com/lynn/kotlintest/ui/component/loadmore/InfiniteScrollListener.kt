package com.lynn.kotlintest.ui.component.loadmore

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by lynnchen on 2019/4/4.
 * Copy from https://github.com/nmillward/UnsplashInfiniteScrollDemo/blob/master/app/src/main/java/com/nickmillward/unsplashdemo/utils/InfiniteScrollListener.java
 * Used to detect and trigger onLoadMore().
 */

abstract class InfiniteScrollListener(private val layoutManager: LinearLayoutManager,
                                      private val minItemsBeforeNextLoad: Int = DEFAULT_MIN_ITEMS_BEFORE_NEXT_LOAD)
    : RecyclerView.OnScrollListener() {

    private var latestTotalItemCount = 0
    private var isLoading = true

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
        val totalItemCount = layoutManager.itemCount

        // Assume list was invalidated -- set back to default.
        if (totalItemCount < latestTotalItemCount) {
            this.latestTotalItemCount = totalItemCount
        }

        // If still loading and data set size has been updated,
        // replaceItems load state and last item count.
        if (isLoading && totalItemCount > latestTotalItemCount) {
            isLoading = false
            latestTotalItemCount = totalItemCount
        }

        // If not loading and within threshold limit, increase current page and load more data.
        if (!isLoading && lastVisibleItemPosition + minItemsBeforeNextLoad > totalItemCount) {
            onLoadMore()
            isLoading = true
        }
    }

    abstract fun onLoadMore()

    companion object {

        const val DEFAULT_MIN_ITEMS_BEFORE_NEXT_LOAD = 20
    }
}