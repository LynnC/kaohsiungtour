package com.lynn.kotlintest.ui.component.loadmore

import android.view.LayoutInflater
import android.view.ViewGroup

import com.lynn.kotlintest.R

import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by lynnchen on 2019/4/4.
 */

class LoadMoreViewHolder(@NonNull parent: ViewGroup) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context)
        .inflate(R.layout.item_loading, parent, false))
