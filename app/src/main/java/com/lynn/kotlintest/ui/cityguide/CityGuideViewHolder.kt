package com.lynn.kotlintest.ui.cityguide

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lynn.kotlintest.data.Data
import com.lynn.kotlintest.data.DataType
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_data.*

/**
 * Created by lynnchen on 2019/4/4.
 */
internal class CityGuideViewHolder(override val containerView: View?)
    : RecyclerView.ViewHolder(containerView!!), LayoutContainer {

    fun bind(data: Data) {
        if (data.type == DataType.INFO) {
            layoutInfo.visibility = View.VISIBLE
            ivLargeImage.visibility = View.GONE

            tvTitle.text = data.title
            tvDescription.text = data.description
            Glide.with(ivLargeImage.context)
                    .load(data.imageUri)
                    .centerCrop()
                    .into(ivSmallImage)

            ivLargeImage.setImageBitmap(null)

        } else if (data.type == DataType.IMAGE) {
            layoutInfo.visibility = View.GONE
            ivLargeImage.visibility = View.VISIBLE

            Glide.with(ivLargeImage.context)
                    .load(data.imageUri)
                    .centerCrop()
                    .into(ivLargeImage)
        }
    }
}
