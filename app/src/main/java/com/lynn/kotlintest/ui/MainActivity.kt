package com.lynn.kotlintest.ui

import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.test.espresso.IdlingResource
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.lynn.kotlintest.R
import com.lynn.kotlintest.ui.cityguide.CityGuideFragment
import com.lynn.kotlintest.ui.cityguide.CityGuidePresenterImpl
import com.lynn.kotlintest.utils.idlingresource.EspressoIdlingResource
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_tab.*

class MainActivity : AppCompatActivity(), TabLayout.OnTabSelectedListener {

    private lateinit var sectionsPagerAdapter: SectionsPagerAdapter

    private val onPageChangeListener = object : ViewPager.SimpleOnPageChangeListener() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            tabLayout.getTabAt(position)?.select()
        }
    }

    @VisibleForTesting
    val countingIdlingResource: IdlingResource
        get() = EspressoIdlingResource.idlingResource

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        tabLayout.addOnTabSelectedListener(this)
        viewPager.adapter = sectionsPagerAdapter
        viewPager.offscreenPageLimit = tabLayout.tabCount
        viewPager.addOnPageChangeListener(onPageChangeListener)
    }

    override fun onTabSelected(tab: TabLayout.Tab) {
        viewPager.setCurrentItem(tab.position, true)
    }

    override fun onTabUnselected(tab: TabLayout.Tab) {
        // Do nothing.
    }

    override fun onTabReselected(tab: TabLayout.Tab) {
        // Do nothing.
    }

    override fun onDestroy() {
        super.onDestroy()
        tabLayout.removeOnTabSelectedListener(this)
        viewPager.removeOnPageChangeListener(onPageChangeListener)
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            val fragment = supportFragmentManager.findFragmentByTag(position.toString())
            if (fragment != null) {
                return fragment
            }

            return when (position) {
                TabIndex.CITY_GUIDE -> {
                    val cityGuideFragment = CityGuideFragment()
                    CityGuidePresenterImpl(cityGuideFragment)
                    cityGuideFragment
                }

                TabIndex.SHOP -> PlaceholderFragment.newInstance("Shop page")
                TabIndex.EAT -> PlaceholderFragment.newInstance("Eat page")
                else -> PlaceholderFragment.newInstance("Invalidate page")
            }
        }

        // Show 3 total pages.
        override fun getCount() = 3
    }
}
