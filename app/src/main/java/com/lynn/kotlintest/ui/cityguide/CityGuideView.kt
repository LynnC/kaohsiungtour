package com.lynn.kotlintest.ui.cityguide

import com.lynn.kotlintest.data.Data
import com.lynn.kotlintest.ui.BaseView

/**
 * Created by lynnchen on 2019/4/4.
 */

interface CityGuideView : BaseView<CityGuidePresenter> {

    fun appendDataList(dataList: List<Data>)

    fun replaceDataList(dataList: List<Data>)

    fun showRefreshing()

    fun showLoadingMore()

    fun dismissRefreshing()

    fun dismissLoadingMore()

    fun networkNotAvailable()
}
