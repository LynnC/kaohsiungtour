package com.lynn.kotlintest.ui

interface BasePresenter {

    fun subscribe()

    fun unsubscribe()
}
