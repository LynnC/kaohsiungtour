package com.lynn.kotlintest.ui.cityguide

import com.lynn.kotlintest.ui.BasePresenter

/**
 * Created by lynnchen on 2019/4/4.
 */

interface CityGuidePresenter : BasePresenter {

    fun refreshDataList()

    fun loadMore(lastDataId: String)
}
