package com.lynn.kotlintest.ui.component.loadmore;

/**
 * Created by lynnchen on 2019/4/3.
 */

public @interface ItemViewType {
    int GENERAL = 0;
    int LOADING = 1;
}
