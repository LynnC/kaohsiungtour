package com.lynn.kotlintest.ui

interface BaseView<T> {

    fun isAttached(): Boolean

    fun setPresenter(presenter: T)
}
