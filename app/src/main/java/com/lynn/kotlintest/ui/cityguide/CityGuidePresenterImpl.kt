package com.lynn.kotlintest.ui.cityguide


import com.lynn.kotlintest.MyApplication
import com.lynn.kotlintest.data.source.Repository
import com.lynn.kotlintest.scheduler.BaseSchedulerProvider
import com.lynn.kotlintest.utils.idlingresource.EspressoIdlingResource
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * Created by lynnchen on 2019/4/4.
 */

class CityGuidePresenterImpl @Inject
constructor(private val view: CityGuideView) : CityGuidePresenter {

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var schedulerProvider: BaseSchedulerProvider

    private val compositeDisposable = CompositeDisposable()

    private val finalAction = {
        if (!EspressoIdlingResource.idlingResource.isIdleNow) {
            EspressoIdlingResource.decrement() // Set app as idle.
        }
    }

    init {
        MyApplication.appComponent!!.inject(this)

        view.setPresenter(this)
    }

    override fun subscribe() {
        refreshDataList()
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
    }

    override fun refreshDataList() {
        view.showRefreshing()

        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.
        EspressoIdlingResource.increment() // App is busy until further notice

        val disposable = repository.getCityGuideDataList()
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .doFinally(finalAction)
                .subscribe(
                        { dataList ->
                            if (view.isAttached()) {
                                view.dismissRefreshing()
                                view.replaceDataList(dataList)
                            }
                        }
                ) {
                    if (view.isAttached()) {
                        view.dismissRefreshing()
                        view.networkNotAvailable()
                    }
                }

        compositeDisposable.add(disposable)
    }

    override fun loadMore(lastDataId: String) {
        view.showLoadingMore()

        // The network request might be handled in a different thread so make sure Espresso knows
        // that the app is busy until the response is handled.
        EspressoIdlingResource.increment() // App is busy until further notice

        val disposable = repository.getCityGuideDataList(lastDataId)
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .doFinally(finalAction)
                .subscribe(
                        { dataList ->
                            if (view.isAttached()) {
                                view.appendDataList(dataList)
                            }
                        }
                ) {
                    if (view.isAttached()) {
                        view.dismissLoadingMore()
                        view.networkNotAvailable()
                    }
                }

        compositeDisposable.add(disposable)
    }
}
