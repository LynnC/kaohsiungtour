package com.lynn.kotlintest.ui.cityguide

import android.view.LayoutInflater
import android.view.ViewGroup

import com.lynn.kotlintest.R
import com.lynn.kotlintest.data.Data
import com.lynn.kotlintest.ui.component.loadmore.ItemViewType
import com.lynn.kotlintest.ui.component.loadmore.LoadMoreAdapter

import androidx.recyclerview.widget.RecyclerView

/**
 * Created by lynnchen on 2019/4/4.
 */

class CityGuideAdapter : LoadMoreAdapter<Data>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == ItemViewType.LOADING) {
            return super.onCreateViewHolder(parent, viewType)
        }

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_data, parent, false)
        return CityGuideViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == ItemViewType.LOADING) {
            return
        }

        val cityGuideViewHolder = holder as CityGuideViewHolder
        getItem(position)?.let { cityGuideViewHolder.bind(it) }
    }
}
