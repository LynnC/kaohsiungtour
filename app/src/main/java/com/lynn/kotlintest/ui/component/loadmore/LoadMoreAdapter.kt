package com.lynn.kotlintest.ui.component.loadmore

import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.VisibleForTesting
import androidx.recyclerview.widget.RecyclerView
import java.util.*


/**
 * Created by lynnchen on 2019/4/4.
 *
 *
 * RecyclerView.Adapter does not support addFooterView(), so we need a custom Adapter.
 * LoadMoreAdapter supports having only one loading footer view.
 * It dismisses loading view when append/replace items.
 */

abstract class LoadMoreAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var showLoading = false

    // Null is loading more item.
    private val items = ArrayList<T?>()

    /**
     * Get the item count excluded the load more item.
     *
     * @return the item count excluded the load more item.
     */
    @VisibleForTesting
    fun getItemOnlyCount() = if (showLoading) items.size - 1 else items.size

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, i: Int): RecyclerView.ViewHolder =
            LoadMoreViewHolder(parent)

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) =
            if (position >= itemCount || items[position] == null) ItemViewType.LOADING else ItemViewType.GENERAL

    fun getItem(position: Int) = if (items.size > position) items[position] else null

    fun showLoadingView(recyclerView: RecyclerView) {
        if (showLoading) {
            return
        }
        showLoading = true
        items.add(null)

        // Cannot call notifyItemInserted() in a scroll callback, so use RecyclerView.post(Runnable).
        recyclerView.post { notifyItemInserted(items.size - 1) }
    }

    fun dismissLoadingView() {
        if (showLoading) {
            showLoading = false
            items.removeAt(items.size - 1)
            notifyItemRemoved(items.size - 1)
        }
    }

    fun appendItems(newItems: List<T>) {
        dismissLoadingView()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun replaceItems(newItems: List<T>) {
        dismissLoadingView()
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }
}
