package com.lynn.kotlintest.ui;

/**
 * Created by lynnchen on 2019/4/4.
 */

public @interface TabIndex {
    int CITY_GUIDE = 0;
    int SHOP = 1;
    int EAT = 2;
}
