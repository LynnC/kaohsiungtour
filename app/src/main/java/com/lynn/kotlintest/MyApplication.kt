package com.lynn.kotlintest

import android.app.Application

import com.lynn.kotlintest.di.AppComponent
import com.lynn.kotlintest.di.AppModule
import com.lynn.kotlintest.di.DaggerAppComponent
import com.lynn.kotlintest.di.DataSourceModule

/**
 * Created by lynnchen on 2019/4/7.
 */

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .dataSourceModule(DataSourceModule())
                .build()
    }

    companion object {

        var appComponent: AppComponent? = null
    }
}