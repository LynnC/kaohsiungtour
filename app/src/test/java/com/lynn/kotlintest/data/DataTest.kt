package com.lynn.kotlintest.data

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test


/**
 * Created by lynnchen on 2019/4/5.
 */
class DataTest {

    var imageData = Data("id", imageUri = "ImageUri")

    var infoData = Data("id", "Title", "Description", "ImageUri")

    @Test
    fun isEqual_differentDataType() {
        assertThat(imageData, not(infoData))
    }

    @Test
    fun isEqual_differentKindOfObject() {
        val anotherObject = String()
        assertThat(infoData, not<Any>(anotherObject))
    }

    @Test
    fun isEqual_imageData() {
        val imageDataClone = Data("id", imageUri = "ImageUri")
        assertThat(imageData, `is`(imageDataClone))

        val imageDataDifferentId = Data("xxx", imageUri = "ImageUri")
        assertThat(imageData, not(imageDataDifferentId))

        val imageDataDifferentImageUri = Data("id", imageUri = "xxx")
        assertThat(imageData, not(imageDataDifferentImageUri))
    }

    @Test
    fun isEqual_infoData() {
        val infoDataClone = Data("id", "Title", "Description", "ImageUri")
        assertThat(infoData, `is`(infoDataClone))

        val infoDataDifferentId = Data("xxx", "Title", "Description", "ImageUri")
        assertThat(infoData, not(infoDataDifferentId))

        val infoDataDifferentTitle = Data("id", "xxx", "Description", "ImageUri")
        assertThat(infoData, not(infoDataDifferentTitle))

        val infoDataDifferentDescription = Data("id", "Title", "xxx", "ImageUri")
        assertThat(infoData, not(infoDataDifferentDescription))

        val infoDataDifferentImageUri = Data("id", "Title", "Description", "xxx")
        assertThat(infoData, not(infoDataDifferentImageUri))
    }
}