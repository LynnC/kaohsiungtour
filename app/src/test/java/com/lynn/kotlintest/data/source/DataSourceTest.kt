package com.lynn.kotlintest.data.source

import com.google.common.collect.Lists
import com.lynn.kotlintest.data.Data
import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers.anyInt
import org.mockito.Matchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import kotlin.test.assertTrue

/**
 * Created by lynnchen on 2019/4/4.
 */
class DataSourceTest {

    private lateinit var repository: Repository

    private lateinit var dataTestSubscriber: TestSubscriber<List<Data>>

    @Mock
    lateinit var remoteDataSource: DataSource

    @Before
    fun setRepository() {
        MockitoAnnotations.initMocks(this)

        repository = Repository.getInstance(remoteDataSource)

        dataTestSubscriber = TestSubscriber()
    }

    @After
    fun destroyRepositoryInstance() {
        Repository.destroyInstance()
    }

    @Test
    fun getDataList_checkDataType() {
        // Given that the remote data source has data available
        val dataList = Lists.newArrayList(
                Data("0", "Title0", "Description0", "ImageUri0"),
                Data("1", "Title1", "Description1", "ImageUri1"),
                Data("2", imageUri = "ImageUri2"))
        setDataList(remoteDataSource, dataList)

        val expectedDataList = Lists.newArrayList(
                Data("0", "Title0", "Description0", "ImageUri0"),
                Data("1", "Title1", "Description1", "ImageUri1"),
                Data("2", imageUri = "ImageUri2"))

        // When subscription is set
        repository.getCityGuideDataList().subscribe(dataTestSubscriber)

        // Then tasks were requested from remote sources
        verify<DataSource>(remoteDataSource).getCityGuideDataList(anyInt())
        dataTestSubscriber.assertValue(dataList)
        assertThat(dataTestSubscriber.valueCount(), `is`(1))

        // Check if match the expected data
        for (data in expectedDataList) {
            assertTrue(dataList.contains(data))
        }
    }

    @Test
    fun getDataList_checkDataType_withLastDataId() {
        // Given that the remote data source has data available
        val dataList = Lists.newArrayList(
                Data("0", "Title0", "Description0", "ImageUri0"),
                Data("1", "Title1", "Description1", "ImageUri1"),
                Data("2", imageUri = "ImageUri2"))
        setDataListWithLastDataId(remoteDataSource, dataList)

        val expectedDataList = Lists.newArrayList(
                Data("0", "Title0", "Description0", "ImageUri0"),
                Data("1", "Title1", "Description1", "ImageUri1"),
                Data("2", imageUri = "ImageUri2"))

        // When subscription is set
        val anyRawStringAsLastDataId = ""
        repository.getCityGuideDataList(anyRawStringAsLastDataId).subscribe(dataTestSubscriber)

        // Then tasks were requested from remote sources
        verify<DataSource>(remoteDataSource).getCityGuideDataList(anyInt(), anyString())
        dataTestSubscriber.assertValue(dataList)

        // Check if match the expected data
        for (data in expectedDataList) {
            assertTrue(dataList.contains(data))
        }
    }

    @Test
    fun getDataList_whenNetworkNotAvailable() {
        // Given that the network are not available
        setNetworkNotAvailable(remoteDataSource)

        // When subscription is set
        repository.getCityGuideDataList().subscribe(dataTestSubscriber)

        // Then tasks were requested from remote sources
        verify<DataSource>(remoteDataSource).getCityGuideDataList(anyInt())

        // And an exception were thrown
        dataTestSubscriber.assertError(Throwable::class.java)
    }

    private fun setDataList(dataSource: DataSource, tasks: List<Data>) {
        `when`(dataSource.getCityGuideDataList(anyInt()))
                .thenReturn(Flowable.just(tasks))
    }

    private fun setDataListWithLastDataId(dataSource: DataSource, tasks: List<Data>) {
        `when`(dataSource.getCityGuideDataList(anyInt(), anyString()))
                .thenReturn(Flowable.just(tasks))
    }

    private fun setNetworkNotAvailable(dataSource: DataSource) {
        `when`(dataSource.getCityGuideDataList(anyInt()))
                .thenReturn(Flowable.error<List<Data>> { Throwable() })
    }
}