package com.lynn.kotlintest.ui.component.loadmore;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by lynnchen on 2019/4/5.
 */

public class InfiniteScrollListenerTest {

    int minItemsBeforeNextLoad = 10;
    int itemCount = 30;

    @Mock
    LinearLayoutManager linearLayoutManager;

    @Mock
    RecyclerView recyclerView;

    InfiniteScrollListener infiniteScrollListener;

    InfiniteScrollListener spy;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        infiniteScrollListener = new InfiniteScrollListener(linearLayoutManager, minItemsBeforeNextLoad) {
            @Override
            public void onLoadMore() {
            }
        };

        spy = Mockito.spy(infiniteScrollListener);

        when(linearLayoutManager.getItemCount()).thenReturn(itemCount);
    }

    @Test
    public void onLoadMore_notTrigger() {
        // Given
        int lastVisibleItemPosition = itemCount - minItemsBeforeNextLoad;
        when(linearLayoutManager.findLastVisibleItemPosition()).thenReturn(lastVisibleItemPosition);

        // When
        spy.onScrolled(recyclerView, 0, 0);

        // Then
        verify(spy, never()).onLoadMore();
    }

    @Test
    public void onLoadMore_trigger() {
        // Given
        int lastVisibleItemPosition = itemCount - minItemsBeforeNextLoad + 1;
        when(linearLayoutManager.findLastVisibleItemPosition()).thenReturn(lastVisibleItemPosition);

        // When
        spy.onScrolled(recyclerView, 0, 0);

        // Then
        verify(spy).onLoadMore();
    }
}