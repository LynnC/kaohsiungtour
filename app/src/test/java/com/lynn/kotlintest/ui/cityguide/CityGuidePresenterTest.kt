package com.lynn.kotlintest.ui.cityguide

import android.content.Context
import com.google.common.collect.Lists
import com.lynn.kotlintest.MyApplication
import com.lynn.kotlintest.data.Data
import com.lynn.kotlintest.data.source.DataSource
import com.lynn.kotlintest.data.source.Repository
import com.lynn.kotlintest.di.AppModule
import com.lynn.kotlintest.di.DaggerAppComponent
import com.lynn.kotlintest.scheduler.BaseSchedulerProvider
import com.lynn.kotlintest.scheduler.ImmediateSchedulerProvider
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Matchers.anyInt
import org.mockito.Matchers.anyString
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


/**
 * Created by lynnchen on 2019/4/4.
 */
class CityGuidePresenterTest {

    @Mock
    lateinit var dataSource: DataSource

    lateinit var repository: Repository

    @Mock
    lateinit var view: CityGuideView

    @Mock
    lateinit var context: Context

    lateinit var presenter: CityGuidePresenter

    @Before
    fun setupMockAndView() {
        MockitoAnnotations.initMocks(this)

        // Prepare dependencies
        val appModule = spy(AppModule(context))
        repository = spy(Repository.getInstance(dataSource))
        `when`(appModule.provideTasksRepository(dataSource)).thenReturn(repository)
        `when`<BaseSchedulerProvider>(appModule.provideSchedulerProvider()).thenReturn(ImmediateSchedulerProvider())
        MyApplication.appComponent = DaggerAppComponent.builder().appModule(appModule).build()

        `when`(view.isAttached()).thenReturn(true)

        presenter = CityGuidePresenterImpl(view)
    }

    @Test
    fun createPresenter_setsThePresenterToView() {
        verify<CityGuideView>(view).setPresenter(presenter)
    }

    @Test
    @Throws(Exception::class)
    fun refreshDataList() {
        // Given that the repository has data available
        val dataList = Lists.newArrayList(
                Data("0", imageUri = "ImageUri0"),
                Data("1", imageUri = "ImageUri1"),
                Data("2", imageUri = "ImageUri2"))

        setDataList(dataList)

        // When
        presenter.refreshDataList()

        // Then
        val inOrder = Mockito.inOrder(view)

        inOrder.verify<CityGuideView>(view).showRefreshing()
        inOrder.verify<CityGuideView>(view).dismissRefreshing()

        verify<Repository>(repository).getCityGuideDataList()
        verify<CityGuideView>(view).replaceDataList(ArgumentMatchers.anyList<Data>())
    }

    @Test
    @Throws(Exception::class)
    fun refreshDataList_whenNetworkNotAvailable() {
        // Given
        setNetworkNotAvailable()

        // When
        presenter.refreshDataList()

        // Then
        val inOrder = Mockito.inOrder(view)

        inOrder.verify<CityGuideView>(view).showRefreshing()
        inOrder.verify<CityGuideView>(view).dismissRefreshing()

        verify<Repository>(repository).getCityGuideDataList()
        verify<CityGuideView>(view, never()).appendDataList(ArgumentMatchers.anyList<Data>())
    }

    @Test
    @Throws(Exception::class)
    fun loadMoreDataList() {
        // Given that the repository has data available
        val dataList = Lists.newArrayList(
                Data("0", imageUri = "ImageUri0"),
                Data("1", imageUri = "ImageUri1"),
                Data("2", imageUri = "ImageUri2"))
        setDataListWithLastDataId(dataList)

        // When
        val anyRealString = ""
        presenter.loadMore(anyRealString)

        // Then
        val inOrder = Mockito.inOrder(view)

        inOrder.verify<CityGuideView>(view).showLoadingMore()
        inOrder.verify<CityGuideView>(view).appendDataList(ArgumentMatchers.anyList<Data>())

        verify<Repository>(repository).getCityGuideDataList(ArgumentMatchers.anyString())
    }

    @Test
    @Throws(Exception::class)
    fun loadMoreDataList_whenNetworkNotAvailable() {
        // Given
        setNetworkNotAvailableWithLastDataId()

        // When
        val anyRealString = ""
        presenter.loadMore(anyRealString)

        // Then
        val inOrder = Mockito.inOrder(view)

        inOrder.verify<CityGuideView>(view).showLoadingMore()
        inOrder.verify<CityGuideView>(view, never()).appendDataList(ArgumentMatchers.anyList<Data>())
        inOrder.verify<CityGuideView>(view).dismissLoadingMore()

        verify<Repository>(repository).getCityGuideDataList(ArgumentMatchers.anyString())
        verify<CityGuideView>(view).networkNotAvailable()
    }

    private fun setDataList(tasks: List<Data>) {
        `when`(repository.getCityGuideDataList()).thenReturn(Flowable.just(tasks))
    }

    private fun setNetworkNotAvailable() {
        `when`(repository.getCityGuideDataList()).thenReturn(Flowable.error<List<Data>>(Throwable()))
    }

    private fun setDataListWithLastDataId(tasks: List<Data>) {
        `when`(repository.getCityGuideDataList(anyString()))
                .thenReturn(Flowable.just(tasks)
                        .concatWith(Flowable.never()))
        `when`(repository.getCityGuideDataList(anyInt(), anyString()))
                .thenReturn(Flowable.just(tasks)
                        .concatWith(Flowable.never()))
    }

    private fun setNetworkNotAvailableWithLastDataId() {
        `when`(repository.getCityGuideDataList(anyString()))
                .thenReturn(Flowable.error<List<Data>> { Throwable() })
        `when`(repository.getCityGuideDataList(anyInt(), anyString()))
                .thenReturn(Flowable.error<List<Data>> { Throwable() })
    }
}