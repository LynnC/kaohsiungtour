package com.lynn.kotlintest.data.source.remote

import android.content.Context
import androidx.annotation.NonNull
import com.google.gson.GsonBuilder
import com.lynn.kotlintest.data.Data
import com.lynn.kotlintest.data.source.DataSource
import io.reactivex.Flowable
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by lynnchen on 2019/4/4.
 *
 *
 * Create fake data from "assets/infinite_datalist.json".
 * Delay half second to emulate network latency before return the value.
 * Assume id is number format.
 */


@Singleton
class FakeRemoteDataSource @Inject
constructor(private val context: Context) : DataSource {

    override fun getCityGuideDataList(count: Int): Flowable<List<Data>> {
        val list = loadFakeJsonDataList(context, count)
        // Delay one second to emulate network latency.
        return Flowable.fromIterable(list!!).toList()
                .delaySubscription(500, TimeUnit.MILLISECONDS)
                .toFlowable()
    }

    override fun getCityGuideDataList(count: Int, @NonNull lastDataId: String): Flowable<List<Data>> {
        val list = loadFakeJsonDataList(context, count)
        // Delay one second to emulate network latency.
        return Flowable.fromIterable(list!!).toList()
                .delaySubscription(500, TimeUnit.MILLISECONDS)
                .toFlowable()
    }

    private fun loadFakeJsonDataList(context: Context, count: Int): List<Data>? {
        try {
            val builder = GsonBuilder()
            val gson = builder.create()
            val array = JSONArray(loadJSONFromAsset(context, "infinite_datalist.json"))
            val dataList = ArrayList<Data>(array.length())
            var i = 0
            while (i < array.length() && i < count) {
                val data = gson.fromJson(array.getString(i), Data::class.java)
                dataList.add(data)
                i++
            }
            return dataList
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }

    }

    private fun loadJSONFromAsset(context: Context, jsonFileName: String): String? {
        val json: String
        val `is`: InputStream
        try {
            val manager = context.assets
            `is` = manager.open(jsonFileName)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charset.forName("UTF-8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }
}
