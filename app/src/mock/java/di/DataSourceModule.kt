package com.lynn.kotlintest.di

import android.content.Context
import com.lynn.kotlintest.data.source.DataSource
import com.lynn.kotlintest.data.source.remote.FakeRemoteDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by lynnchen on 2019/4/7.
 */

@Module
class DataSourceModule {

    @Provides
    @Singleton
    fun provideDataSource(context: Context): DataSource = FakeRemoteDataSource(context)
}
