package com.lynn.kotlintest.ui.cityguide

import android.util.Log
import android.view.View

import com.lynn.kotlintest.R
import com.lynn.kotlintest.ui.MainActivity
import com.lynn.kotlintest.ui.component.loadmore.LoadMoreAdapter

import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId


/**
 * Created by lynnchen on 2019/4/5.
 */
@LargeTest
class CityGuideFragmentTest {

    /**
     * [IntentsTestRule] is an [ActivityTestRule] which inits and releases Espresso
     * Intents before and after each test run.
     *
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @get:Rule
    var activityTestRule = ActivityTestRule(MainActivity::class.java, false, true)

    /**
     * Prepare your test fixture for this test. In this case we register an IdlingResources with
     * Espresso. IdlingResource resource is a great way to tell Espresso when your app is in an
     * idle state. This helps Espresso to synchronize your test actions, which makes tests significantly
     * more reliable.
     */
    @Before
    @Throws(Exception::class)
    fun setUp() {
        IdlingRegistry.getInstance().register(
                activityTestRule.activity.countingIdlingResource)
    }

    /**
     * Unregister your Idling Resource so it can be garbage collected and does not leak any memory.
     */
    @After
    @Throws(Exception::class)
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(
                activityTestRule.activity.countingIdlingResource)
    }

    /**
     * Returns a matcher that matches [RecyclerView] that contains the count is greater than
     * the specific itemCount.
     */
    private fun withGreaterItemCount(itemCount: Int): Matcher<View> {
        return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {}

            override fun matchesSafely(item: RecyclerView): Boolean {
                val adapter = item.adapter
                Log.d("withGreaterItemCount", "itemCount = $itemCount")
                return adapter is LoadMoreAdapter<*> && adapter.getItemOnlyCount() > itemCount
            }
        }
    }

    @Test
    fun testRecyclerView_loadDataList() {
        val recyclerView = activityTestRule.activity.findViewById<RecyclerView>(R.id.rvCityGuide)
        val loadMoreAdapter = recyclerView.adapter as LoadMoreAdapter<*>

        val recyclerViewInteraction = onView(withId(R.id.rvCityGuide))

        var itemCount: Int
        val scrollTimes = 10
        for (i in 0 until scrollTimes) {
            itemCount = loadMoreAdapter.getItemOnlyCount() - 1

            // Scroll to the position of the last item.
            recyclerViewInteraction.perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemCount))

            // Wait until CountingIdlingResource shows idle and then
            // check if the item count is increase.
            recyclerViewInteraction.check(matches(withGreaterItemCount(itemCount)))
        }
    }
}